import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from './book';

@Injectable({providedIn: "root"})
export class BookApiService {

  constructor(private http: HttpClient) {}

  getAll(): Observable<Book[]> {
    return this.http.get<Book[]>(`http://localhost:4730/books`);
  }

  getBookByIsbn(isbn: string): Observable<Book> {
    return this.http.get<Book>(`http://localhost:4730/books/${isbn}`);
  }

  createBook(book: Partial<Book>): Observable<Book> { // TODO: create model for book creation
    return this.http.post<Book>(`http://localhost:4730/books`, book);
  }
}
