import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Book } from '../book';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss'],
})
export class BookCardComponent implements OnInit {
  // @Input() content: Book = {
  //   title: '',
  //   author: '',
  //   abstract: '',
  //   numOfPages: 0,
  // };

  // @Input() content!: Book;

  @Input() content: Book | undefined

  @Output() detailClick = new EventEmitter<Book>();

  customStyle = {
    color: 'gray',
  };

  constructor() {}

  ngOnInit(): void {}

  handleDetailClick(event: MouseEvent) {
    event.preventDefault();
    console.log(event);
    this.detailClick.emit(this.content);
  }
}
