import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  catchError,
  delay,
  EMPTY,
  map,
  Observable,
  Subscription,
  switchMap,
  tap,
  throwError,
} from 'rxjs';
import { Book } from '../book';
import { BookApiService } from '../book-api.service';
import { SavableComponent } from '../confirm-leave.guard';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss'],
})
export class BookDetailComponent implements OnInit, SavableComponent {
  book$: Observable<Book> = EMPTY;

  book: Book | undefined; // only for when we don't want to use async pipe

  error = false;

  constructor(private route: ActivatedRoute, private bookApi: BookApiService) {}
  

  ngOnInit(): void {
    /*
    Snapshot-based access to route params
    (won't work when you navigate from BookDetail to other BookDetail)

    const isbn = this.route.snapshot.params['isbn'];
    const isbn = this.route.snapshot.paramMap.get('isbn');
    const isbn = this.route.snapshot.paramMap.get('isbn');
    if (isbn) {
      this.book$ = this.bookApi.getBookByIsbn(isbn).pipe(
        catchError(e => {
          this.error = true;
          return EMPTY;
        })
      )
    }
    */

    /* 
  
    Watch out for subscribe inside subscribe 
    because it may produce race conditions

    this.route.params.subscribe((params) => {
      const isbn = params['isbn'];
      this.bookApi
        .getBookByIsbn(isbn) // ISBN: 123, ISBN: 456
        .subscribe((book) => {
          this.book = book; // {ISBN: 456}, {ISBN: 123}
        });
    });
    */

    /*
    Unsubscribe from obsolete observables where necessary 

    let bookApiSubscription = Subscription.EMPTY;
    this.route.params.subscribe((params) => { // ISBN: 123, ISBN: 456
      if (bookApiSubscription) {
        bookApiSubscription.unsubscribe();
      }
      const isbn = params['isbn'];
      bookApiSubscription = this.bookApi
        .getBookByIsbn(isbn)
        .subscribe((book) => {
          this.book = book; // {ISBN: 456}
        });
    });
    */

    // or use built-in RxJS operators like switchMap
    this.book$ = this.route.params.pipe(
      map(params => params['isbn']),
      switchMap(isbn => this.bookApi.getBookByIsbn(isbn))
    )
  }
  
  hasUnsavedChanges(): boolean {
    return true;
  }
}
