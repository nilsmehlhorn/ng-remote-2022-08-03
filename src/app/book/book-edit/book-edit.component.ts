import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Book } from '../book';
import { BookApiService } from '../book-api.service';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.scss'],
})
export class BookEditComponent implements OnInit, OnDestroy {
  book: Book | undefined;

  private subscription = new Subscription();

  private subscriptions: Subscription[] = [];

  constructor(private bookApi: BookApiService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    const isbn = this.route.snapshot.params['isbn'];
    const getBookSubscription = this.bookApi
      .getBookByIsbn(isbn)
      .subscribe((book) => {
        this.book = book;
      });
    this.subscription.add(getBookSubscription);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    // OR
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onSubmit(formValue: any): void {
    console.log(formValue);
    // const saveBookSubscription = this.bookApi.save().subscribe();
    // this.subscription.add(saveBookSubscription);
    // // OR
    // this.subscriptions.push(saveBookSubscription);
  }
}
