import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '../book';

@Pipe({
  name: 'bookFilter',
})
export class BookFilterPipe implements PipeTransform {
  transform(books: Book[] | null, query: string): Book[] {
    if (!books) {
      return [];
    }
    if (!query) {
      return books;
    }
    return books.filter((book) =>
      book.title.toLocaleLowerCase().includes(query.toLocaleLowerCase())
    );
  }
}
