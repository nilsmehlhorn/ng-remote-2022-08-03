import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, take } from 'rxjs';
import { Book } from '../book';
import { BookApiService } from '../book-api.service';

@Component({
  selector: 'app-book-new',
  templateUrl: './book-new.component.html',
  styleUrls: ['./book-new.component.scss'],
})
export class BookNewComponent implements OnInit, OnDestroy {
  form = this.fb.nonNullable.group({
    isbn: ['', [Validators.required]],
    title: ['', [Validators.required]],
    author: ['', Validators.required],
    abstract: ['', Validators.required],
    numOfPages: [0, Validators.min(50)],
  });

  private subscription = Subscription.EMPTY;

  constructor(
    private fb: FormBuilder,
    private bookApi: BookApiService
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    const book: Partial<Book> = {
      ...this.form.value,
    };
    this.subscription = this.bookApi.createBook(book).subscribe((created) => {
      console.log(created);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
