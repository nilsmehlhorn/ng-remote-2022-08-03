import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { delay, EMPTY, Observable } from 'rxjs';
import { Book } from './book';
import { BookApiService } from './book-api.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
  // providers: [BookApiService]
})
export class BookComponent implements OnInit, OnDestroy {
  bookSearchTerm = '';

  books$: Observable<Book[]> | undefined;

  constructor(private bookApi: BookApiService, private router: Router) {}

  ngOnInit(): void {
    this.books$ = this.bookApi.getAll().pipe(delay(1000));
  }

  ngOnDestroy(): void {}

  goToBookDetails(book: Book) {
    this.router.navigate(['/books', 'detail', book.isbn]);
  }

  updateBookSearchTerm(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.bookSearchTerm = inputElement.value;
  }
}
