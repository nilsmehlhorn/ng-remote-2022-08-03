export interface Book {
    isbn: string;
    cover: string;
    title: string;
    author: string;
    abstract: string;
    numOfPages: number;
}
