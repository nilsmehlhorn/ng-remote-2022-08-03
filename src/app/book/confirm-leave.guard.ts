import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { BookDetailComponent } from './book-detail/book-detail.component';

export interface SavableComponent {
  hasUnsavedChanges(): boolean;
}

@Injectable({
  providedIn: 'root',
})
export class ConfirmLeaveGuard implements CanDeactivate<SavableComponent> {
  canDeactivate(
    component: SavableComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): boolean {
    const hasUnsavedChanges = component.hasUnsavedChanges();
    if (hasUnsavedChanges) {
      return window.confirm('Do you want to leave?');
    }
    return true;
  }
}
